/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alvin
 */

import java.util.Scanner;
import java.util.concurrent.TimeUnit;
public class DivisorApp2 {
    public static void main(String[] args) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        String barrier = "-----------------------------";
        System.out.println("Divisor App");
        System.out.println("");
        System.out.println(barrier);
        System.out.println("");
        long a,b;
        do{
            System.out.print("Masukan angka Pertama: ");
            a = sc.nextLong();
            System.out.println("");
            System.out.print("Masukan angka Kedua: ");
            b = sc.nextLong();
            System.out.println("");
            System.out.println(barrier);
            System.out.println("");
            if(b == 0){
                System.out.println("Angka Kedua tidak boleh 0, silakan coba lagi.");
                System.out.println("");
                System.out.println(barrier);
                System.out.println("");
                TimeUnit.SECONDS.sleep(1);
            }else{
               double start, end;
               start = System.nanoTime();
               long ans = a/b;
               end = System.nanoTime();
                System.out.print("Angka hasil bagi: ");
                System.out.println(ans);
                System.out.println("");
                System.out.print("Waktu yang digunakan sistem: ");
                System.out.println(((end-start)/1000000) + " milidetik.");
                System.out.println("");
                System.out.println(barrier);
                System.out.println(""); 
                System.out.println("Terima kasih telah menggunakan aplikasi ini.");
            }
        }while(b == 0);
    }
}
